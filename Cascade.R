ParticleInfo = setClass(
  "ParticleInfo",
  
  slots = c(
    type= "numeric" , #"gamma"==1, "e-"==2, "e+"==3 
    #type = "character", # "gamma", "e-", "e+" 
    E = "numeric", # Energy, in MeV
    t = "numeric", # Time
    theta = "numeric", # Total scattering angle
    age = "numeric", # Age
    number="numeric",
    depth="numeric"
  ),
  
  # Fill in default values
  prototype = list(type = 2, E = 1000, t = 0, theta = 0, age = 0, depth=0),
  
  # Check for validity after construction
  validity = function(object) {
    return(!((object@type < 1 | object@type > 3) | object@E <= 0 | object@t<=0 | (object@theta < -180 | object@theta > 180) | object@age < 0 | object@number < 0 | object@depth < 0))
  }
)

#p1=ParticleInfo(type=1,E=2,t=3,theta=4,age=5,number=6)
#p2=ParticleInfo(type=2,E=2,t=3,theta=4,age=5,number=1)
#p3=ParticleInfo(type=2,E=2,t=7,theta=4,age=5,number=2, depth=1)
#p4=ParticleInfo(type=3,E=2,t=7,theta=4,age=5,number=3, depth=1)
#x=c(p1,p2,p3, p4)  #x[[2]]

# x- array of ParticleInfo objects
getIndexByType=function(x,type.in)
{
  idx=0
  idx=sapply(c(1:length(x)), FUN=function(i, type){if(type==x[[i]]@type){if(idx[1] == 0) {idx[1]=i} else{idx=c(idx, i)}}}, type=type.in) 
  #print(unlist(idx))
  return (unlist(idx))
}

# x- array of ParticleInfo objects
getIndexByAge=function(x,age.in)
{
  idx=0
  idx=sapply(c(1:length(x)), FUN=function(i, age){if(age==x[[i]]@age){if(idx[1] == 0) {idx[1]=i} else{idx=c(idx, i)}}}, age=age.in) 
  return (unlist(idx))
}

getIndexByDepth=function(x,depth.in)
{
  idx=0
  idx=sapply(c(1:length(x)), FUN=function(i, depth){if(depth==x[[i]]@depth){if(idx[1] == 0) {idx[1]=i} else{idx=c(idx, i)}}}, depth=depth.in) 
  return (unlist(idx))
}

# Tests
#i=getIndexByAge(x,5)
#i=getIndexByAge(x,1)
#is.null(getIndexByAge(x,1)[[1]])
#i=getIndexByType(x,3)
#i=getIndexByType(x,1)
#i=getIndexByType(x,2)
#i=getIndexByDepth(x,0)
#i=getIndexByDepth(x,1)

#E.krit=1
#n=10
#time =seq(1:n)
#p=ParticleInfo(type=1,E=100,t=1,theta=0,age=1,number=1)
iter=function(i, strct)
{
  return (strct[[i]])
} 

trial=function(i,strct, p)
{
  return (rbinom(1,strct[[i]]@number, p))
}

# ph +Air = e^{-} +e^{+}
spawnYule_basic=function(part.atDepth, depth, p.dead)
{
  photons.which = getIndexByType(part.atDepth,1)
  photons= unlist(sapply(photons.which , FUN=iter, strct=part.atDepth))
  #p.survive = p.survive*(1-p.dead)
  #sort by age
  #age.n=c(0:depth) # How to deal with  Different ages
  p=1/(p.dead+1)
  n.dead = unlist(sapply(1:length(photons), FUN=trial, strct=photons, p=1-p))
 # print(n.dead)
#  print(photons)
  surv=unlist(sapply(1:length(photons), FUN=function(i, strct){ return (max(0,strct[[i]]@number-n.dead[i]))}, strct=photons))
 # print("Survived:")
  #print(surv)
#  print("Depth")
#  print(depth)
  for (i in 1:length(photons))
  {
    photons[[i]]@number = 2*surv[i]
    photons[[i]]@depth = photons[[i]]@depth +1
    photons[[i]]@age = 1
    photons[[i]]@t = photons[[i]]@t +1
  }
#  print("Data")
#  print(photons)

    ret= c(photons)
#  else if(length(alive.idx) > 0)
#    ret = photons.new
#  else
#    ret = list(ParticleInfo(type=photons[[i]]@type,E=photons[[i]]@E/2,t=photons[[i]]@t+1,theta=photons[[i]]@theta,age=photons[[i]]@age+1,number=0, depth=photons[[i]]@depth+1))
  
  return (ret)
}



shower.Extention = function(part, depth, p.dead=1/2, isAssym=FALSE)
{
  idx=getIndexByDepth(part, depth)
  pool=part[idx]
  ph.index=getIndexByType(pool,1)

  result=NULL
  if(!is.null(ph.index))
  {
    tmp = spawnYule_basic(pool[ph.index],depth, p.dead )
    if(length(tmp)>0)
      result= tmp
  }
 
  return (result)
}
